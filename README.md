# Gnubies-Tallergit

Taller sobre comandos Git para el curso Gnubies-2021 del grupo linux de la Universidad Distrital , grupo dirigido por el tutor Andrés Becerra. 


## Name
Taller Comando Básicos git .


## Description
En este taller se  utilizan somando basicos sobre git , se aprende la utilizacion basica de commits realizados desde una terminal GNU/Linux, se agregan en la carpeta Imagenes , los comandos utilizados para la creación y modificacion del repositiorio . 

![](https://vabadus.es/images/cache/imagen_nodo/images/articulos/5c9deedea0c7e844300455.png)

## Autor : 
El autor del proyecto es Miguel Fuentes - 20182005007 . 
## Licencia
El proyecto es de uso libre y cuenta con licencia GNU Affero General Public License v3.0.

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:42e2031722595e76646a7f4c94177f53?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:42e2031722595e76646a7f4c94177f53?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:42e2031722595e76646a7f4c94177f53?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/mafuentesr/gnubies-tallergit.git
git branch -M main
git push -uf origin main
```
